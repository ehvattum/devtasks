﻿using System;

using Imdb;

namespace Runner
{
    public class Program
    {
        public void Main(string[] args)
        {
            Console.WriteLine("Hello World");

            var search = new Search()
                .WithFullPlot()
                .WithTomatoRating()
                .ForTitle("Interstellar");

            var result = search.ExecuteAsync().Result;
            Console.WriteLine(result);
            Console.WriteLine(result.Plot);
            Console.WriteLine(result.TomatoConsensus);
            Console.ReadLine();
        }
    }
}
