﻿using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Imdb
{
    public class Search
    {
        private readonly StringBuilder querybuilder;
        private string plotLength = "";
        private string primarySearch;
        private string tomatoes;
        private string type = "";
        private string year = "";


        public Search()
        {
            this.querybuilder = new StringBuilder("http://www.omdbapi.com/?");
        }


        public Search WithType(Type videotype)
        {
            this.type = "&type=" + videotype.ToString().ToLower();
            return this;
        }


        public Search ReleasedIn(int releaseYear)
        {
            this.year = "&y=" + releaseYear;
            return this;
        }


        public Search ForTitle(string title)
        {
            this.primarySearch = "&t=" + title;
            return this;
        }


        public Search WithFullPlot()
        {
            this.plotLength = "&plot=full";
            return this;
        }


        public Search ForId(string id)
        {
            this.primarySearch = "&i=" + id;
            return this;
        }


        public Search WithTomatoRating()
        {
            this.tomatoes = "&tomatoes=true";
            return this;
        }


        public Search For(string query)
        {
            this.primarySearch = "&s=" + query;
            return this;
        }


        public async Task<Result> ExecuteAsync()
        {
            this.querybuilder.Append(this.primarySearch).Append(this.type).Append(this.year).Append(this.plotLength).Append(this.tomatoes).Append("&v=1&r=json");
            var request = (HttpWebRequest)WebRequest.Create(this.querybuilder.ToString());
            request.Accept = "application/json";
            JsonSerializer serializer = new JsonSerializer();
            var response = await request.GetResponseAsync();
            Result data;
            using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            {
                data = (Result)serializer.Deserialize(streamReader, typeof(Result));
            }
            return data;
        }
    }
}