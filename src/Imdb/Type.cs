namespace Imdb
{
    public enum Type
    {
        Movie,
        Series,
        Episode
    }
}